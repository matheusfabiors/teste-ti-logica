<?php

// Conexão com o BD
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mydb";

$conn = mysqli_connect($servername, $username, $password, $dbname);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Consulta
$sql = "SELECT Tb_banco.nome, Tb_convenio.verba, Tb_contrato.codigo, Tb_contrato.data_inclusao, Tb_contrato.valor, Tb_contrato.prazo
        FROM Tb_contrato
        JOIN Tb_convenio_servico ON Tb_contrato.convenio_servico = Tb_convenio_servico.codigo
        JOIN Tb_convenio ON Tb_convenio_servico.convenio = Tb_convenio.codigo
        JOIN Tb_banco ON Tb_convenio.banco = Tb_banco.codigo";

$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    // Saída de dados
    while($row = mysqli_fetch_assoc($result)) {
        echo "Nome do Banco: " . $row["nome"] . " - Verba: " . $row["verba"] . " - Código do Contrato: " . $row["codigo"] . " - Data de Inclusão: " . $row["data_inclusao"] . " - Valor: " . $row["valor"] . " - Prazo: " . $row["prazo"] . "<br>";
    }
} else {
    echo "Nenhum resultado encontrado.";
}

mysqli_close($conn);

?>
