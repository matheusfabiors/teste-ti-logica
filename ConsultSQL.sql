SELECT Tb_banco.nome as g, Tb_convenio.verba as g, 
       MIN(Tb_contrato.data_inclusao) as g, MAX(Tb_contrato.data_inclusao) as g, 
       SUM(Tb_contrato.valor) as g
FROM Tb_banco 
INNER JOIN Tb_convenio 
    ON Tb_banco.codigo = Tb_convenio.banco 
INNER JOIN Tb_convenio_servico 
    ON Tb_convenio.codigo = Tb_convenio_servico.convenio 
INNER JOIN Tb_contrato 
    ON Tb_convenio_servico.codigo = Tb_contrato.convenio_servico 
GROUP BY Tb_banco.nome, Tb_convenio.verba;
